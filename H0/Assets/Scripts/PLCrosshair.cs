﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLCrosshair : MonoBehaviour
{
    // control crosshair


    [SerializeField] private float speed = 20.0f;
    [SerializeField] private float randspeed = 5.0f;
    [SerializeField] private Joystick joystick;
    [SerializeField] private float joystickMax;

    Rigidbody2D rb;
    Vector2 moveVelocity;
    Vector2 randmoveVelocity;
    Vector2 moveInput;


    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody2D>();
        StartCoroutine(RandomMove());
        joystickMax = joystick.HandleRange - joystick.DeadZone;
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = new Vector2(.0f, .0f);
        moveInput += new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveInput += new Vector2(joystick.Horizontal/joystickMax , joystick.Vertical/joystickMax);
        
        moveVelocity = moveInput * speed;

    }


    void FixedUpdate() {
        rb.MovePosition(rb.position + (randmoveVelocity+moveVelocity) * Time.fixedDeltaTime);
    }

    IEnumerator RandomMove(){
        while(true){
            yield return new WaitForSeconds(0.3f);
            //random shacking
            Vector2 randmove = new Vector2(Random.Range(-0.1f, 0.1f),Random.Range(-0.1f, 0.1f));
            randmoveVelocity = randmove.normalized * randspeed;
        }
    }
}
