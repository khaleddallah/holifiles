﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CfjTrigEnd : StateMachineBehaviour
{
    // Determine When the first jump animation completed 

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject b = GameObject.FindGameObjectWithTag("FJS");
        b.GetComponent<Cfj>().FJEnd = true;
    }


}
