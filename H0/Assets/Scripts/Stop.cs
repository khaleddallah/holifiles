﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stop : MonoBehaviour
{
    // functions of the stop button in the game scene
    
    [SerializeField] private GameObject stopMenu;
    [SerializeField] private GameObject stopBtn;


    public void StopButton(){
        stopBtn.SetActive(false);
        stopMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ContinueTime(){
        Time.timeScale = 1f;
    }

    public void Resume(){
        stopBtn.SetActive(true);
        stopMenu.SetActive(false);
    }


    public void QuitGame(){
        Application.Quit();
    }


    public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadLevel(string x){
        SceneManager.LoadScene(x);
    }
}
