﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SM : MonoBehaviour
{
    // Score Manager
    
    public float health = 100.0f;
    public float lifetime = 60.0f;

    [SerializeField] private TextMeshProUGUI lifetimeText;
    [SerializeField] private TextMeshProUGUI textWinLose;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private GameObject healthObject;
    [SerializeField] private GameObject lifetimeObject;
    [SerializeField] private float refHealth;
    [SerializeField] private float refLifetime;
    [SerializeField] private float dangerHealth;
    [SerializeField] private float dangerLifetime;

    //Object has to stop when win/lose
    [SerializeField] private GameObject crosshair;
    [SerializeField] private GameObject pet;
    [SerializeField] private GameObject winLoseGui;

    bool oneTime=true;
    IEnumerator decreaseT;

    void Start()
    {
        decreaseT = DecreaseLT();
        StartCoroutine(decreaseT);

        refHealth = health;
        refLifetime = lifetime;

        dangerHealth = 0.35f*refHealth;
        dangerLifetime = 0.35f*refLifetime;

    }

    // Update is called once per frame
    void Update()
    {
        lifetimeText.text = lifetime.ToString("0");
        healthSlider.value = health; 
        
        if( (health<=0 || lifetime<=0) && oneTime){
            oneTime=false;
            Lose();
        }

        CheckDanger();
    }


    public void Hurt()
    {
        health -= 0.07f;
    }

    public IEnumerator DecreaseLT(){
        while(true){
            lifetime -= Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator WinLose(string x){
        winLoseGui.SetActive(true);

        Time.timeScale = 0f;

        lifetimeObject.GetComponent<Animator>().enabled = false;
        healthObject.GetComponent<Animator>().enabled = false;

        crosshair.SetActive(false);
        pet.GetComponent<SpriteRenderer>().color = new Color32(255,255,255,255);
        pet.GetComponent<Animator>().enabled = false;

        textWinLose.text = x;
        yield return null;
        // while(true){
        //     textWinLose.enabled = !textWinLose.enabled;
        //     yield return new WaitForSeconds(0.3f);
        // }
    }

    public void Lose(){
        textWinLose.color = new Color32(255, 83, 75, 255);
        StartCoroutine(WinLose("LOSE"));
        StopCoroutine(decreaseT);
    }

    public void Win(){
        textWinLose.color = new Color32(245, 255, 97, 255);
        StartCoroutine(WinLose("WIN"));
        StopCoroutine(decreaseT);
    }



    public void  CheckDanger(){
        if(health<dangerHealth){
            healthObject.GetComponent<Animator>().enabled = true;
            dangerHealth = -1 ;
        }
        if(lifetime<dangerLifetime){
            lifetimeObject.GetComponent<Animator>().enabled = true;
            dangerLifetime = -1 ;

        }

    }


}
