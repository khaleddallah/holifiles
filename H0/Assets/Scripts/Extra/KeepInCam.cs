﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepInCam : MonoBehaviour
{
    // Used with PLCrosshair to keep it inside the view screen
    Camera camm;

    void Start(){
        camm = Camera.main;
    }


    void Update() {
        Vector3 pos = camm.WorldToViewportPoint(transform.position);
        if(pos.x>0.95f){
            pos.x = 0.95f;
            transform.position = camm.ViewportToWorldPoint(pos);
        } 
        if(pos.x<0.05f){
            pos.x = 0.05f;
            transform.position = camm.ViewportToWorldPoint(pos);
        }
        if(pos.y>0.9f){
            pos.y = 0.9f;
            transform.position = camm.ViewportToWorldPoint(pos);
        } 
        if(pos.y<0.1f){
            pos.y = 0.1f;
            transform.position = camm.ViewportToWorldPoint(pos);
        }
    }
    
}
