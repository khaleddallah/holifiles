﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;

public class CameraShake : MonoBehaviour
{
    // used by PLGun, when gun fire (etime=duration).

    public float duration = 0.3f;
    public float amp = 1.2f;
    public float freq = 2.0f;
    public float etime = 0f;

    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    [SerializeField] private CinemachineBasicMultiChannelPerlin virtualCameraNoise;

    void Start()
    {
        virtualCameraNoise = virtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
    }


    void Update()
    {
        if(etime>0){
            virtualCameraNoise.m_AmplitudeGain = amp;
            virtualCameraNoise.m_FrequencyGain = freq;
            etime -= Time.deltaTime;
        }
        else{
            virtualCameraNoise.m_FrequencyGain = 0f;
            virtualCameraNoise.m_AmplitudeGain = 0f;
        }
    }
}
