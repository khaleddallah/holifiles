﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraBorder : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera vc;
    [SerializeField] private GameObject cam;
    [SerializeField] private GameObject pet;

    [SerializeField] Transform petcam;

    BoxCollider2D col;
    public float top, btm, left, right;

    Vector3 topLeft, topRight, btmLeft, btmRight;
    Color ctb,crl; 
    bool firstTime=true;
    // Start is called before the first frame update
    void Start()
    {
        // vc = GetComponent<CinemachineVirtualCamera>();
        petcam.position = pet.transform.position;
    }


    void Update()
    {
        if(vc.enabled){
            if(firstTime){
                firstTime = false;
                col = GetComponent<BoxCollider2D>();
                Vector2 size = col.size;
                Vector3 worldPos = transform.position + new Vector3(col.offset.x, col.offset.y, 0f);
                top = worldPos.y + (size.y / 2f) - vc.m_Lens.OrthographicSize;
                btm = worldPos.y - (size.y / 2f) + vc.m_Lens.OrthographicSize;
                left = worldPos.x - (size.x / 2f) + vc.m_Lens.OrthographicSize * vc.m_Lens.Aspect;
                right = worldPos.x + (size.x /2f) - vc.m_Lens.OrthographicSize * vc.m_Lens.Aspect;
                
                topLeft = new Vector3(left,top,0f);
                topRight = new Vector3(right,top,0f);
                btmLeft = new Vector3(left,btm,0f);
                btmRight = new Vector3(right,btm,0f);
                ctb = new Color(125f,0.0f,125f,255f);
                crl = new Color(125f,0.0f,125f,255f);
                

            }
        

            
            if(cam.transform.position.x <= left){
                petcam.position = new Vector3(left, pet.transform.position.y, 0f);
                crl.b=.0f;
                // Debug.DrawLine(topLeft,  btmLeft,  crl);            
            }
            else if(cam.transform.position.x >= right){
                petcam.position = new Vector3(right, pet.transform.position.y, 0f);
                crl.b=.0f;
                // Debug.DrawLine(topRight, btmRight, crl);
            }
            else{
                petcam.position = pet.transform.position;
                crl.b=125f;
                // Debug.DrawLine(topRight, btmRight, crl);
                // Debug.DrawLine(topLeft,  btmLeft,  crl);            
            }

            if(cam.transform.position.y >= top){
                petcam.position = new Vector3(pet.transform.position.x, top, 0f);
                ctb.b=.0f;
                // Debug.DrawLine(topLeft,  topRight, ctb);
            }
            else if(cam.transform.position.y <= btm){
                petcam.position = new Vector3(pet.transform.position.x, btm, 0f);
                ctb.b=.0f;
                // Debug.DrawLine(btmLeft,  btmRight, ctb);
            }
            else{
                petcam.position = pet.transform.position;
                ctb.b=125f;
                // Debug.DrawLine(btmLeft,  btmRight, ctb);
                // Debug.DrawLine(topLeft,  topRight, ctb);
            }

            // vc.transform.position = petcam.position;


        }
    
        }
}
