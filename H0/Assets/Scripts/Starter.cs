﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Starter : MonoBehaviour
{
    // used for the start button in the starter scene
    [SerializeField] private GameObject loadScreen;
    [SerializeField] private Slider slider;
    [SerializeField] private TextMeshProUGUI sliderValue;


    public void LoadLevel(string x){
        StartCoroutine(LoadAsynchronously(x));
    }

    IEnumerator LoadAsynchronously(string x){
        loadScreen.SetActive(true);

        AsyncOperation operation = SceneManager.LoadSceneAsync(x);


        while(!operation.isDone){
            slider.value = operation.progress/.9f;
            sliderValue.text = (slider.value*100).ToString("0.0") + " %";
            yield return null;
        }
    }

}
