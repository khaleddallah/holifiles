﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CfjBigRockTrig : MonoBehaviour
{
    // Determine if the hole touch the bigRock
    public bool trigged = false;

    bool firstTime = true;

    void OnTriggerEnter2D(Collider2D other) {
        if(firstTime){
            firstTime = false;
            return;
        }
        else{
            trigged = true;
        }
    }
}
