﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    // When Intro Video completed, switch to the other scene.

    [SerializeField] private VideoPlayer videoPlayer;

    void Update()
    {
        if(videoPlayer.isPaused){
            transform.gameObject.SetActive(false);
            SceneManager.LoadScene("Starter");
        }
    }


}
