﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SMHealthbar : MonoBehaviour
{
    // control healthbar
    
    Quaternion temp;
    [SerializeField] private Gradient gradient;
    [SerializeField] private Image fill; 
    [SerializeField] private Slider slider;

    void Start()
    {
        temp = transform.rotation;
        fill.color = gradient.Evaluate(1f);
    }

    void Update()
    {
        transform.rotation = temp;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
