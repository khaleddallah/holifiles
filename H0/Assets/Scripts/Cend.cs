﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cend : MonoBehaviour
{
    // Used to Determine if the Pet dig until the other side of the rock
    // Then Control it to reach the end automatically 
    // And when reach the end, it call "win" func

    [SerializeField] private PetController petController;
    [SerializeField] private Transform endPlatform;
    [SerializeField] private Transform petTarget;    
    [SerializeField] private Vector3 tempTarget;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private SM scoreManager;

    RaycastHit2D hit;
    Vector3 direction;
    Vector3 distance;

    void Start()
    {
        tempTarget = petTarget.position;
    }

    void Update()
    {
        if(petTarget.position != tempTarget){
            distance = petTarget.position - tempTarget;
            direction = distance.normalized;
            hit = Physics2D.Raycast(petTarget.position, direction);
            // Debug.DrawLine(petTarget.position, hit.point, Color.green, 2f);
            if(hit.transform.name == transform.name){
                petController.curTarget  = endPlatform.transform.position;
            }
            tempTarget = petTarget.position;

        }
    }


    private void OnCollisionEnter2D(Collision2D other) {
        if(other.transform.CompareTag("Player")){
            scoreManager.Win();
        }
    }



}
