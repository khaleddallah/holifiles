﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLGun : MonoBehaviour
{
    // control the gun

    [SerializeField] private Transform crosshair;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject bulletStarter;
    [SerializeField] private float bulletSpeed = 70.0f;
    [SerializeField] private CameraShake cameraShake;
    [SerializeField] private GameObject destroyBlock;
    [SerializeField] private GameObject destroyBlockV2;
    [SerializeField] private GameObject maskDad;
    [SerializeField] private GameObject petTarget;
    [SerializeField] private GameObject tempHole;
    [SerializeField] private Cfj cfj;

    bool canFire=true;

    public bool disableS = false;
    public bool permenantDisableS = false;


    void Update()
    {
        if(disableS){
            tempHole.transform.GetChild(3).gameObject.SetActive(false);
            disableS = false;
        }

        if(Input.GetButtonDown("Fire1")){
            WhenFire();
       }
    }


    public void WhenFire(){
        // Shake Camera
        cameraShake.etime=cameraShake.duration;

        Vector3 diff = crosshair.position - bulletStarter.transform.position;
        float rotz = Mathf.Atan2(diff.y,diff.x)*Mathf.Rad2Deg;    
        float distance = diff.magnitude;
        if(canFire)
            StartCoroutine(fireBullet(diff.normalized, rotz, crosshair.position, distance));

    }



    IEnumerator fireBullet(Vector2 direction, float rotz, Vector3 target, float distance){
        canFire=false;
        GameObject b = Instantiate(bulletPrefab) as GameObject;
        b.transform.position = bulletStarter.transform.position;
        b.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotz+90);
        b.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
        while(true){
            if( (b.transform.position-bulletStarter.transform.position).magnitude >= distance){
                MakeDestroy(target);
                Destroy(b);
                break;
            }
            yield return null;
        }
    }

    void MakeDestroy(Vector3 target){
        // Instantiate destroyBlock
        GameObject b;
        if(permenantDisableS){
            b = Instantiate(destroyBlockV2) as GameObject;
        }
        else{
            b = Instantiate(destroyBlock) as GameObject;
        }
        b.transform.parent = maskDad.transform;
        b.transform.position = new Vector3 (target.x, target.y, 0.0f);
        b.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(-170.0f, 180.0f));
        tempHole = b;

        // Set NewTarget
        petTarget.transform.position = b.transform.position;
        canFire=true;
    }

    
}
