﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SMLifeTime : MonoBehaviour
{   
    // control lifetime bar

    [SerializeField] private TextMeshProUGUI lifetimeText;
    [SerializeField] private Gradient gradient;
    [SerializeField] private SM sm;

    float initValue;

    void Start()
    {
        lifetimeText.color = gradient.Evaluate(1f);
        initValue = sm.lifetime;
    }

    void Update()
    {
        lifetimeText.color = gradient.Evaluate(sm.lifetime/initValue);

    }


}
