﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMObstacle : MonoBehaviour
{
    // control what happened when the pet toutch the obstacles
    
    [SerializeField] private SM sm;
    [SerializeField] private Animator pet;

    private void OnTriggerStay2D(Collider2D other) {
        if(other.transform.name=="PetAICustom2"){
            pet.SetTrigger("hurt");
            if(sm.health>0){
                sm.Hurt();
            }
            else{
                sm.health=0;
            }
        }
    }

}
