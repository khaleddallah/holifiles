﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cfj : MonoBehaviour
{

    [SerializeField] private Transform starterPoint;
    [SerializeField] private Transform target;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private GameObject customPet;
    [SerializeField] private GameObject pet;
    [SerializeField] private PLGun gun;
    [SerializeField] private CfjBigRockTrig bigRockTrigger;

    public bool holeTrig=false;
    public bool SFJTrig=false;
    public bool SFJTrigHasTrigged=false;
    public bool FJEnd = false;
    public bool isEnable = true;
    public float fjumpForce=70f;

    
    Vector3 currentTargetPosition;

    void Start()
    {
        currentTargetPosition = target.position;
    }

    void Update()
    {
        if(isEnable){
            // Check Trigger
            if(!SFJTrigHasTrigged && currentTargetPosition != target.position){
                currentTargetPosition=target.position;
                StartCoroutine(IsHitHole());
            }

            // First Jump 
            if(SFJTrig){
                customPet.GetComponent<Animator>().SetTrigger("FjEn");
                customPet.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, fjumpForce, 0), ForceMode2D.Impulse);
                /* pet.GetComponent<Animator>().SetBool("jump",true); */
                SFJTrig = false;
                gun.permenantDisableS = true;
                customPet.GetComponent<PetController>().SetAllTarget(currentTargetPosition);
                customPet.GetComponent<PetController>().bigRockCollider.enabled = false;
            }

            /*
            if(FJEnd){
                customPet.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                customPet.GetComponent<Animator>().enabled = false;
                Debug.Log("FJEnd@@@");
                pet.GetComponent<Animator>().SetBool("FjEn",false);
            }
            */

            if(customPet.GetComponent<PetController>().reach){
                /* customPet.transform.rotation = new Quaternion( 0f, 0f, 0f, 0f); */
                isEnable = false;
            }


        }
        else{
            // Disable hole collider ( which effect pet lately if not disabled)
            gun.permenantDisableS=true;
        }
    }

    IEnumerator IsHitHole(){
        Vector3 direction = (currentTargetPosition - starterPoint.position).normalized;
        RaycastHit2D hitInfo = Physics2D.Raycast(starterPoint.position, direction);
        gun.disableS=true;

        // check if the hole accessible from pet position (not inside bigRock) | "s" is the collider of the hole
        if (hitInfo.transform.name == "s") {
            holeTrig=true;
        }
        else {
            holeTrig=false;
        }
        // check if the hole touch the edge of the bigRock
        if (holeTrig && bigRockTrigger.trigged){
            SFJTrig=true;
            SFJTrigHasTrigged = true;
        }
        else{
            bigRockTrigger.trigged=false;
        }

        yield return null;
    }
}
