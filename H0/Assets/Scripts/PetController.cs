﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
    // control the Pet 

    [SerializeField] private Transform petTarget;
    [SerializeField] private Cfj cfj;
    [SerializeField] private float distanceFollow = 3.0f;
    [SerializeField] private Transform centerPoint;
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private LayerMask layer0;
    [SerializeField] private float jumpForce=100f; 
    [SerializeField] private float reachReff=1.0f;

    float groundDistance;
    float jumpReff;
    bool canJump=true;
    Animator animator;
    RaycastHit2D hit;
    Rigidbody2D rb;
    bool firstime=true;
    Vector3 lastTarget;
    Vector3 tempTarget;
    public Vector3 curTarget;
    Vector3 diff;
    float dis;

    public bool reach = false;
    public PolygonCollider2D bigRockCollider;

    void Start()
    {
        // curTarget  = new Vector3();
        // tempTarget = new Vector3();
        // lastTarget = new Vector3();

        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        SetAllTarget(transform.position);
        reach=false;
        jumpReff = distanceFollow*Mathf.Tan(Mathf.Deg2Rad*30f);
        groundDistance = Physics2D.Raycast(transform.position,new Vector3(0f,-1f,0f)).distance;
        groundDistance += groundDistance*0.2f;
    }

    void FixedUpdate()
    {
        if(cfj.FJEnd){
            // Debug.DrawLine(
                // new Vector3(curTarget.x-0.2f,curTarget.y,curTarget.z),
                // new Vector3(curTarget.x+0.2f,curTarget.y,curTarget.z),
                // new Color(255f,0f,0f,255f));


            // Stay on the first target until reach it
            if(firstime && reach){
                firstime = false;
            }

            if(!firstime && petTarget.position!=tempTarget){
                lastTarget=tempTarget;
                tempTarget=petTarget.position;
                if( ((tempTarget.x-transform.position.x)>=distanceFollow) && NoRockObs(tempTarget) && NotHigher(tempTarget) ){
                    curTarget=tempTarget;
                    reach = false;
                }
            }                    
            PetMoving(); 
        }
    }

    void PetMoving(){
        diff.x = curTarget.x - transform.position.x;
        if(diff.x<0){
            transform.rotation = new Quaternion(0f,180f,0f,0f);
        }
        else{
            transform.rotation = new Quaternion(0f,0f,0f,0f);
        }

        dis = Mathf.Abs(diff.x);


        if(dis>0 && !reach){
            if(dis<=reachReff){
                reach = true;
            }

            if( (curTarget.y-transform.position.y) > (jumpReff) ){
                animator.SetBool("jump",true);
                if( (Physics2D.Raycast(transform.position,new Vector3(0f,-1f,0f)).distance) <= groundDistance ){
                    if(canJump){
                        canJump=false;
                        rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode2D.Impulse);
                        StartCoroutine(CanJumpTimer());
                    }
                }
            }
            else{
                animator.SetBool("running",true);
            }
        }
        else {
            animator.SetBool("running",false);
            animator.SetBool("jump",false);

        }
    }


    public void SetAllTarget(Vector3 x ){
        lastTarget = tempTarget = curTarget = x;
    }

    public bool NoRockObs(Vector3 newTarget){
        Vector3 dis = newTarget - lastTarget;
        hit = Physics2D.Raycast(lastTarget, dis.normalized,  Mathf.Infinity);
        // Debug.DrawLine(lastTarget, hit.point, Color.blue, 10f);

        if(hit.distance<dis.magnitude){
            return false;
        }
        else{
            return true;
        }

    }

    public bool NotHigher(Vector3 newTarget){
        return true;
    }

    IEnumerator CanJumpTimer(){
        yield return new WaitForSeconds(1.2f);
        canJump=true;
    }

}
